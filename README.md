DISCLAIMER: This program uses your current Internet connection in order to download NSFW content. By using the program (HiA.rb), you're giving your consent about its effect.

ATTENTION: Ce programme utilise votre connexion Internet afin de télécharger du contenu NSFW. En utilisant ce programme (HiA.rb), vous consentez à ce que le programme fasse cela.

# HiA (FR)

Ce programme crée le dossier "Tags" dans son propre dossier, puis y crée un dossier pour chaque "Tag" disponible sur le site (NSFW) "nhentai.net". Ensuite, il crée un dossier pour chaque doujinshi trouvé et enfin,
il télécharge toutes les images liées directement à ce doujinshi pour les mettre dans ce dossier. Il crée aussi un fichier texte nommé "Récapitulatif.txt" dans lequel se trouve l'URL du doujinshi ainsi que son nombre de pages.

## Comment l'utiliser

**Il vous faudra:**

* Une connexion Internet ayant accès à "nhentai.net"
* Un ordinateur
* Ruby sur votre ordinateur
* La "Gem" "HTTParty" sur votre ordinateur
* La "Gem" "Nokogiri" sur votre ordinateur

Le programme n'a pas été testé en dehors de Windows 10, je ne garantis donc pas que le programme marche comme il faut sur un autre OS.

Pour utiliser ce programme, vous devez allez dans la console de commande, faire "cd [CHEMIN DU PROGRAMME]" puis "HiA.rb". Cette dernière commande lance le programme.

## Précisions

Vous pouvez aussi faire un double-clic sur "HiA.rb" pour le lancer, mais dans le cas où il crash, il n'y aura aucun moyen de savoir pourquoi, je ne le recommande pas.

Le programme continuera de télécharger jusqu'à ce qu'il ait fini, buggé ou crash peu importe la mémoire de votre appareil ou la connexion utilisée.

Je n'ai pas pu testé si le programme peut fonctionner à 100% (télécharger toutes les images de tous les doujinshis du site) par manque de temps, de bonne connexion et de mémoire. Dans le cas où vous rencontrez un soucis,
contactez-moi avec les 25 dernières lignes écrites par la console avant le soucis.

Le programme commence par les tags de façon alphabétique ET par les doujinshis par popularité. Ainsi, les premiers doujinshis qui apparaîtront seront ceux avec le tag "3d" qui sont relativement populaires.

# HiA (EN)

This program creates the "Tags" folder in its own, then creates in "Tags" one folder for each "Tag" available on the (NSFW) website "nhentai.net". It then creates a folder for each doujinshi it found and finally,
it downloads all images that are directly related to this doujinshi so it can put them in its folder. It also creates a text file named "Récapitulatif.txt" in which can be found the doujinshi's URL and number of pages, in french.

## How to use it

**You will need:**

* An Internet connection that can access "nhentai.net"
* A computer
* Ruby on your computer
* The "HTTParty" "Gem" on this computer
* The "Nokogiri" "Gem" on this computer

This program has not been tested outside of Windows 10, so I do not guarantee that it will work as intended on any other OS.

To use this program, go to the command console, do "cd [PROGRAM LOCATION]" then "HiA.rb". This last command will launch the program.

## Precisions

You are able to double-click "HiA.rb" to launch it directly, but in the case it crashes, you will not be able to figure out how, so I do not recommend this method.

The program will continue to download until it is done, bugged or has crashed no matter your computer's memory or the connection it is using.

I was not able to test the program enough to figure out if it works fully (if it can download all doujinshis on the website) due to my lack of time, memory and good connection. If you were to encounter an issue, please
contact me with the last 25 lignes that were printed out by the command console.

The program starts out with tags in alphabetic order AND popular doujinshis, so the first doujinshis that will appearon your computer will be those with the "3d" tag that are pretty popular.