require 'nokogiri'
require 'httparty'
require "open-uri"

$page = 1
$lol = 1
$doujin_numero = 1
$tag_doujin_page = 1
$website = "https://nhentai.net"
main_directory = Dir.mkdir("Tags") unless File.exists?("Tags")
Dir.chdir("Tags")

def fetching_tags
	$website_tags = $website + "/tags/?page=" + $page.to_s
	unparsed_http = HTTParty.get($website_tags)
	parsed_http = Nokogiri::HTML(unparsed_http.body)
	tags = parsed_http.css("a.tag")
	if tags.to_s.include? "e" #If the page actually features a tag or not
		for $tag in tags #For each tag within the page
			$tag = $tag.to_s
			$tag = $tag[$tag.index("/")..]
			$tag = $tag[$tag.index("/")..$tag.index('"') - 1]
			actual_tag = $tag[5...-1]
			puts("\n----------")
			puts("\nTAG ACTUEL: " + actual_tag)
			tag_directory = Dir.mkdir(actual_tag) unless File.exists?(actual_tag)
			Dir.chdir(actual_tag)
			def fetching_doujinshis
				webtag = $website + $tag + "popular" + "?page=" + $tag_doujin_page.to_s
				puts("URL ACTUELLE: " + webtag)
				unparsed_tag_http = HTTParty.get(webtag)
				parsed_tag_http = Nokogiri::HTML(unparsed_tag_http.body)
				$doujins = parsed_tag_http.css("a.cover")
				if $doujins.to_s.include? "e" #If the tag actually features a doujinshi or not
					for doujin in $doujins #For each doujinshis within the tag
						doujin = doujin.to_s
						doujin = doujin[doujin.index("g")..]
						doujin = "/" + doujin[doujin.index("g")..doujin.index('"') - 1]
						doujin_id = doujin[doujin.index("g") + 2...-1]
						web_doujin = $website + doujin
						puts("DOUJIN ACTUEL: " + web_doujin)
						unparsed_doujin = HTTParty.get(web_doujin)
						parsed_doujin = Nokogiri::HTML(unparsed_doujin.body)
						doujin_pages_nombre = 0
						doujin_pages = parsed_doujin.css("a.gallerythumb")
						for pages in doujin_pages #Counts the number of pages in the doujinshi
							doujin_pages_nombre = doujin_pages_nombre + 1
						end
						#doujin_pages_nombre = 1 #TO TEST THE FOLDER CREATION OUT
						doujin_directory = Dir.mkdir(doujin_id.to_s) unless File.exists?(doujin_id.to_s)
						Dir.chdir(doujin_id.to_s)
						recap = File.new("Récapitulatif.txt", "w")
						recap.write("URL du doujinshi: " + web_doujin)
						recap.write("\nNombre de pages: " + doujin_pages_nombre.to_s)
						recap.close
						puts("Nombre de pages: " + doujin_pages_nombre.to_s)
						page_du_doujin = 1
						doujin_pages_nombre.times do #For each page the doujinshi has
							unparsed_image = HTTParty.get(web_doujin + page_du_doujin.to_s)
							parsed_image = Nokogiri::HTML(unparsed_image.body)
							$doujin_image_url = parsed_image.css("img.fit-horizontal")
							$doujin_image_url = $doujin_image_url.to_s
							begin
								$doujin_image_url = $doujin_image_url[$doujin_image_url.index('"') + 1..$doujin_image_url.index('jpg') + 2]
							rescue #Prevents a stupid fatal error from occuring
								puts("Cette image est en format png !")
								$doujin_image_url = $doujin_image_url[$doujin_image_url.index('"') + 1..$doujin_image_url.index('png') + 2]
							end
							puts("IMAGE ACTUELLE: " + $doujin_image_url)
							open($doujin_image_url) {|f|
								File.open(page_du_doujin.to_s + '.jpg', "wb") do |file|
									file.puts f.read #Puts the good shit in the good folder
								end
							}
							page_du_doujin = page_du_doujin + 1 #The image that will get fetched in a second will be different from the one before!!!
						end
						$doujin_numero = $doujin_numero + 1 #Changes which doujin the program's at
						if File.basename(Dir.getwd) != "Tags" #just. in. case.
							Dir.chdir("../") #Makes it so doujinshis aren't put within doujinshis, would be kinda dumb tbh..
						end
					end
					$tag_doujin_page = $tag_doujin_page + 1 #Changes the page within the tag
					fetching_doujinshis #In the case every doujinshi in the page (before change ^) has been treated, check the one after for more
				end
				if File.basename(Dir.getwd) != "Tags" #Prevents the program to put doujinshis everywhere in your computer lol
					Dir.chdir("../")
				end
				$tag_doujin_page = 1 #Because we're no longer in the same tag, we can put that value back to 1
				$lol = $lol + 1 #Changes the doujin
			end
			fetching_doujinshis #Execute the above code
		end
		$lol = 1 #Because we're no longer in the same page, the program should look into the first doujin of the new page first
		$page = $page + 1 #Changes the tag page
		puts("\nPage " + $page.to_s + ":")
		fetching_tags
	else
		puts("Il n'y a plus d'autres tags, merci d'avoir utilisé ce programme !")
	end
	rescue SocketError => error_message
		puts "Le programme n'a pas pu se connecter au site web. Vérifiez votre connexion."
		puts "\nPour référence, voici le message d'erreur:\n" + error_message.to_s
end

fetching_tags
